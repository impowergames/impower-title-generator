import { transformTerms, unpackSpecificAndAdjacentTerms } from "./getTerms";

export const getCleanedTagTerms = (tagTerms: { [tag: string]: string[] }) => {
  const cleanedTerms: { [tag: string]: string[] } = {};
  const organizedTerms: { [tag: string]: string[] } = {};

  Object.entries(tagTerms).forEach(([tag, terms]) => {
    organizedTerms[tag] = Array.from(new Set(terms.sort()));
  });

  Object.entries(organizedTerms)
    .sort()
    .forEach(([tag, terms]) => {
      const { specific, adjacent } = unpackSpecificAndAdjacentTerms(
        tag,
        tagTerms
      );
      const transformedSpecific = transformTerms(specific);
      const transformedAdjacent = transformTerms(adjacent);
      cleanedTerms[tag] = terms
        .filter(
          (term) =>
            !transformedAdjacent.includes(term) &&
            transformedSpecific.filter((t) => t === term).length < 2
        )
        .map((term) =>
          !term.startsWith(">") && term.includes(" ") && !term.includes("_")
            ? `_${term}_`
            : term
        )
        .sort();
    });

  return cleanedTerms;
};
