import axios from "axios";
import cheerio from "cheerio";
import tagTerms from "../input/tagTerms.json";
import { unpackTag } from "./getTerms";

const url = "https://www.phrases.org.uk/phrase-thesaurus"; // URL we're scraping
const AxiosInstance = axios.create(); // Create a new Axios Instance

const scrapePhrases = async (
  word: string,
  match: "exact" | "related"
): Promise<{ [tag: string]: string[] }> => {
  const phrases = {};
  try {
    // Send an async HTTP Get request to the url
    const response = await AxiosInstance.get(`${url}/${match}/${word}.html`);

    const html = response.data; // Get the HTML from the HTTP request
    const $ = cheerio.load(html); // Load the HTML string into cheerio
    const unorderedListElement: Cheerio = $("ul > li");

    unorderedListElement.each((i, elem) => {
      const phrase = $(elem).text();
      if (!phrases[word]) {
        phrases[word] = [];
      }
      phrases[word].push(phrase);
    });
  } catch {
    console.warn(`${match} match not found: ${word}`);
  }
  return phrases;
};

export const scrapeTagPhrases = async (
  tags: string[]
): Promise<{
  results: { [tag: string]: string[] };
  noResults: string[];
}> => {
  const promises: Promise<{ [tag: string]: string[] }>[] = [];
  const terms = tags.flatMap((tag) => unpackTag(tag, tagTerms));
  const uniqueTerms = Array.from(new Set([...terms])).sort();

  const noResults = new Set<string>();

  uniqueTerms.forEach((term) => {
    promises.push(scrapePhrases(term, "exact"));
    promises.push(scrapePhrases(term, "related"));
  });
  const allPhrases = await Promise.all(promises);
  const termResults: { [term: string]: string[] } = {};
  allPhrases.forEach((phrases) => {
    const phraseEntries = Object.entries(phrases);
    phraseEntries.forEach(([key, value]) => {
      if (!termResults[key]) {
        termResults[key] = [];
      }
      value.forEach((phrase) => {
        if (!termResults[key].includes(phrase)) {
          termResults[key].push(phrase);
        }
      });
    });
  });
  const tagResults: { [tag: string]: string[] } = {};
  tags.forEach((tag) => {
    const terms = unpackTag(tag, tagTerms);
    const phrases = terms
      ? terms
          .flatMap((term) => {
            {
              const results = termResults[term];
              if (!results?.length) {
                noResults.add(term);
              }
              return results;
            }
          })
          .filter((term) => Boolean(term))
      : [];
    if (!tagResults[tag]) {
      tagResults[tag] = [];
    }
    tagResults[tag] = Array.from(
      new Set([...tagResults[tag], ...phrases])
    ).sort();
  });
  return {
    results: tagResults,
    noResults: Array.from(noResults).sort(),
  };
};
