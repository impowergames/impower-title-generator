import fs from "fs";
import tagTerms from "../input/tagTerms.json";
import { keywords } from "../generated/keywords";
import { termVectors } from "../generated/termVectors";
import { getRelatedTerms } from "../utils/getRelatedTerms";

getRelatedTerms(tagTerms, Object.keys(keywords), termVectors, 0.4, 4).then(
  (result) => {
    const definition =
      "export const relatedTerms: { [tag: string]: string[] } = ";
    const path = "./src/tmp/relatedTerms.ts";

    fs.writeFile(path, definition + JSON.stringify(result), (err) => {
      if (err) {
        console.log("FAILED!", err);
      } else {
        console.log("EXPORTED TO: ", path);
      }
    });
  }
);
