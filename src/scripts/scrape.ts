import fs from "fs";
import tagTerms from "../input/tagTerms.json";
import { scrapeTagPhrases } from "../utils/scrapeTagPhrases";

const allTags = Object.keys(tagTerms);
scrapeTagPhrases(allTags)
  .then(({ results, noResults }) => {
    const phrasesDefinition = `export const scrapedPhrases: { [tag: string]: string[] } = `;
    const resultsPath = "src/tmp/scrapedPhrases.ts";
    fs.writeFile(
      resultsPath,
      phrasesDefinition + JSON.stringify(results),
      (err) => {
        if (err) {
          console.log("FAILED!", err);
        } else {
          console.log("DONE!");
        }
      }
    );
    const noResultPath = "src/output/no-results.ts";
    const noResultsDefinition = `export const noResults: string[] = `;
    fs.writeFile(
      noResultPath,
      noResultsDefinition + JSON.stringify(noResults),
      (err) => {
        if (err) {
          console.log("FAILED!", err);
        } else {
          console.log("DONE!");
        }
      }
    );
  })
  .catch((err) => {
    console.error(err);
  });
